const express = require('express')
const router = express.Router()

let path = require('path')
let mongo = require(path.resolve('./mongoOperations'))

router.get('/seasons', function (req, res) {
    mongo.fetchYear().then((seasons) => {
        res.send(seasons)
    })
})

router.get('/seasons/:season', function (req, res) {
    const season = req.params.season
    mongo.fetchCityForYear(season).then((cities) => {
        res.send(cities)
    }).catch( (err) => {
        console.log(err)
    })
})

router.get('/seasons/:season/cities/:city', function (req, res) {
    const season = req.params.season
    const city = req.params.city
    mongo.fetchTeamsForCity(season, city).then((teams) => {
        res.send(teams)
    }).catch( (err) => {
        console.log(err)
    })
})

router.get('/seasons/:season/cities/:city/teams/:team', function (req, res) {
    const season = req.params.season
    const city = req.params.city
    const team = req.params.team
    mongo.fetchDetails(season, city, team).then((details) => {
        res.send(details)
    }).catch( (err) => {
        console.log(err)
    })
})

router.get('/seasons/:season/cities/:city/teams/:team/players/:player', function (req, res) {
    const season = req.params.season
    const city = req.params.city
    const team = req.params.team
    const player = req.params.player
    mongo.playerDetails(player).then((details) => {
        res.send(details)
    }).catch( (err) => {
        console.log(err)
    })
})

module.exports = router
