const path = require('path')
const connectToMongo = require(path.resolve('./mongoConn.js'))

function fetchYear() {
    return new Promise((resolve, reject) => {
        connectToMongo.mongoConn('data').then((mongo) => {
            mongo.matches.distinct('season', (err, data) => {
                if (err) {
                    console.log(err)
                }
                mongo.conn.close()
                resolve(data)
            })
        }).catch((err) => {
            console.log(err)
        })
    })
}

function fetchCityForYear(seasonVal) {
    return new Promise((resolve, reject) => {
        connectToMongo.mongoConn('data').then((mongo) => {
            season = Number(seasonVal)
            mongo.matches.distinct('city', {
                season
            }, (err, data) => {
                if (err) {
                    console.log(err)
                }
                mongo.conn.close()
                resolve(data)
            })
        }).catch((err) => {
            console.log(err)
        })
    })
}

function fetchTeamsForCity(seasonVal, city) {
    return new Promise((resolve, reject) => {
        connectToMongo.mongoConn('data').then((mongo) => {
            season = Number(seasonVal)
            mongo.matches.distinct('winner', {
                season,
                city
            }, (err, data) => {
                if (err) {
                    console.log(err)
                }
                mongo.conn.close()
                resolve(data)
            })
        }).catch((err) => {
            console.log(err)
        })
    })
}

function fetchDetails(seasonVal, city, winner) {
    return new Promise((resolve, reject) => {
        connectToMongo.mongoConn('data').then((mongo) => {
            season = Number(seasonVal)
            mongo.matches.aggregate([{
                $match: {
                    season,
                    city,
                    winner
                }
            }, {
                $lookup: {
                    from: 'deliveries',
                    localField: 'winner',
                    foreignField: 'batting_team',
                    as: 'deliveries'
                }
            }, {
                $unwind: '$deliveries'
            }, {
                $group: {
                    '_id': '$deliveries.batsman'
                }
            }]).toArray((err, data) => {
                if (err) {
                    console.log(err)
                }
                mongo.conn.close()
                resolve(data)
            })
        }).catch((err) => {
            console.log(err)
        })
    })
}

function playerDetails(batsman) {
    return new Promise((resolve, reject) => {
        connectToMongo.mongoConn('data').then((mongo) => {
            mongo.deliveries.distinct('dismissal_kind', {
                batsman
            }, (err, data) => {
                if (err) {
                    console.log(err)
                }
                mongo.conn.close()
                resolve(data)
            })
        }).catch((err) => {
            console.log(err)
        })
    })
}

module.exports = {
    fetchYear,
    fetchCityForYear,
    fetchTeamsForCity,
    fetchDetails,
    playerDetails
}
