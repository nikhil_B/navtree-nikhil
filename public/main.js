let hello = $('<div></div>')
$('body').append(hello)
hello.append('Hello, Click Here for the Seasons')
hello.attr('id', 'Hello')

function getSeasons() {
    return $.ajax({
        url: "/api/seasons/",
        type: "GET"
    })
}

getSeasons().done((seasons) => {
    $('#Hello').click((event) => {
        let season = $('<ul></ul>')
        if ($('body').children('ul').length !== 0) {
            $('body').children('ul').remove()
        } else {
            $('#Hello').html('<b>Seasons</b>')
            for (i of seasons) {
                let seasonVal = $('<li></li>')
                seasonVal.attr('id', i)
                seasonVal.attr('class', 'seasons')
                seasonVal.append(i)
                season.append(seasonVal)
            }
            $('body').append(season)
        }
    })
})

function getCities(season) {
    return $.ajax({
        url: `/api/seasons/${season}`,
        type: 'GET'
    })
}

$('body').on('click', 'li.seasons', function (event) {
    event.stopPropagation()
    const season = event.target
    getCities(season.id).done((city) => {
        let cities = $('<ul><b>Cities</b></ul>')
        if ($(season).next('ul').length == 1) {
            $(season).next().remove()
        } else {
            for (i of city) {
                let cityVal = $('<li></li>')
                cityVal.attr('id', i)
                cityVal.attr('class', 'city')
                cityVal.append(i)
                cities.append(cityVal)
            }
            cities.insertAfter(season)
        }
    })
})

function getTeamsForCities(season, city) {
    return $.ajax({
        url: `/api/seasons/${season}/cities/${city}`,
        type: 'GET'
    })
}

$('body').on('click', 'li.city', function (event) {
    event.stopPropagation()
    const city = event.target
    const season = $(city).parent().prev()[0].id
    getTeamsForCities(season, city.id).done((cityData) => {
        let teams = $('<ul><b>Teams</b></ul>')
        if ($(city).next('ul').length == 1) {
            $(city).next().remove()
        } else {
            for (i of cityData) {
                let teamVal = $('<li></li>')
                teamVal.attr('id', i)
                teamVal.attr('class', 'team')
                teamVal.append(i)
                teams.append(teamVal)
            }
            teams.insertAfter(city)
        }
    })
})

function getPlayerDetails(season, city, team) {
    return $.ajax({
        url: `/api/seasons/${season}/cities/${city}/teams/${team}`,
        type: 'GET'
    })
}

$('body').on('click', 'li.team', function (event) {
    event.stopPropagation()
    const team = event.target
    const city = $(team).parent().prev()[0]
    const season = $(city).parent().prev()[0].id
    getPlayerDetails(season, city.id, team.id).done((allPlayers) => {
        let players = $('<ul><b>Players for/against</b></ul>')
        if ($(team).next('ul').length == 1) {
            $(team).next().remove()
        } else {
            for (i of allPlayers) {
                let playerVal = $('<li></li>')
                playerVal.attr('id', i._id)
                playerVal.attr('class', 'player')
                playerVal.append(i._id)
                players.append(playerVal)
            }
            players.insertAfter(team)
        }
    })
})

function finalDetails(season, city, team, player) {
    return $.ajax({
        url: `/api/seasons/${season}/cities/${city}/teams/${team}/players/${player}`,
        type: 'GET'
    })
}

$('body').on('click', 'li.player', function (event) {
    event.stopPropagation()
    const player = event.target
    const team = $(player).parent().prev()[0]
    const city = $(team).parent().prev()[0]
    const season = $(city).parent().prev()[0].id
    finalDetails(season, city.id, player.id, player.id).done((playerDetails) => {
        let details = $('<ul><b>dismissal throughout the seasons</b></ul>')
        console.log(player.id)
        console.log(playerDetails)
        if ($(player).next('ul').length == 1) {
            $(player).next().remove()
        } else {
            for (i of playerDetails) {
                let detail = $('<li></li>')
                detail.attr('id', i)
                detail.attr('class', 'dismissal')
                detail.append(i)
                details.append(detail)
            }
            details.insertAfter(player)
        }
    })
})
