function mongoConn(database) {
  const MongoClient = require('mongodb').MongoClient;
  const url = 'mongodb://localhost:27017';
  return new Promise((resolve, reject) => {
    MongoClient.connect(url, {
      useNewUrlParser: true,
    }, (err, conn) => {
      if (err) {
        console.log('WARN: start the mongodb service!');
        reject(err.message);
      } else {
        // console.log('Connection Successful')
        const dataset = conn.db(database);
        const matches = dataset.collection('matches');
        const deliveries = dataset.collection('deliveries');
        resolve({
          matches,
          deliveries,
          conn,
        });
      }
    });
  });
}

module.exports = {
  mongoConn,
};