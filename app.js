const express = require('express')
const app = express()

app.set('view engine', 'ejs')
app.use(express.static('styles'))
app.use(express.static('public'))
app.use('/api', require('./routes/api'))


app.get('/', function(req, res) {
    res.render('index')
})

app.listen(3000, function () {
    console.log('listening on 3000!')
})
